package rest

import (
  "net/http"
  "strings"
  "encoding/json"
)

func HandlerSpecies (w http.ResponseWriter, r *http.Request) {
  parts := strings.Split(r.URL.Path, "/")        // /conservation/v2/country/..
  key  := parts[4]                               // {:Key}
  var sp Species                                 // Declares a Species struct

    // URLs for species and year discovered
  url_species := "https://api.gbif.org/v1/species/" + key + "/"
  url_year    := "https://api.gbif.org/v1/species/" + key + "/name/"

    // Checks for error when decoding REST api endpoint from URL
  checkDecoding(w, json.NewDecoder(recieveApi(w, url_species)).Decode(&sp))
  checkDecoding(w, json.NewDecoder(recieveApi(w, url_year)).Decode(&sp))

    // Formats the text into application/JSON
  sp.format(w)
}
