package main

import (
    "fmt"
    "log"
    "net/http"
    "os"
    "rest"
)

func main() {

    port := os.Getenv("PORT")
    if port == "" {

        port = "8080"
    }

    http.HandleFunc("/conservation/v1/country/", rest.HandlerCountry)
    http.HandleFunc("/conservation/v1/species/", rest.HandlerSpecies)
    http.HandleFunc("/conservation/v1/diag/", rest.HandlerDiag)

    fmt.Println("Listening on port " + port)
    log.Fatal(http.ListenAndServe(":"+port, nil))

}
