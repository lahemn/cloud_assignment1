package rest

import (
  "io"
  "net/http"
  "fmt"
  "encoding/json"
)

type Country struct {
  Code        string    `json:"alpha2Code"`
  Name        string    `json:"name"`
  Flag        string    `json:"flag"`
  Species     []string  `json:"species"`
  SpeciesKey  []int     `json:"speciesKey"`
}

type Speciesid struct {
  Results     []struct {
    Species     string     `json:"species"`
    SpeciesKey  int        `json:"speciesKey"`
    }    `json:"results"`
}

type Species struct {
  Key             int         `json:"key"`
  Kingdom         string      `json:"kingdom"`
  Phylum          string      `json:"phylum"`
  Order           string      `json:"order"`
  Family          string      `json:"family"`
  Genus           string      `json:"genus"`
  ScientificName  string      `json:"scientificName"`
  CanonicalName   string      `json:"canonicalName"`
  Year            string      `json:"bracketYear"`
}

type Diag struct {
  Gbif            int        `json:"gbif"`
  Restcountries   int        `json:"restcountries"`
  Version         string     `json:"version"`
  Uptime          float64    `json:"uptime"`
}

func recieveApi(w http.ResponseWriter, url string) io.Reader {
  resp, err := http.Get(url)
  if err != nil {
    http.Error(w, "Could not request server", http.StatusNotFound)
  }
  return resp.Body
}

func checkDecoding(w http.ResponseWriter, err error) {
  if err != nil {
    http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
  }
}

func (co *Country) duplicate (s string) bool {
  for _, lc := range co.Species {
    if lc == s {
      return true
    }
  }
  return false
}

func (co Country) format(w http.ResponseWriter) {
  json_co, err := json.MarshalIndent(co, "", "    ")
  if err != nil {
    http.Error(w, "Could not encode the object", http.StatusBadRequest)
  }
  fmt.Fprint(w, string(json_co))
}

func (sp Species) format(w http.ResponseWriter) {
  json_sp, err := json.MarshalIndent(sp, "", "    ")
  if err != nil {
    http.Error(w, "Could not encode the object", http.StatusBadRequest)
  }
  fmt.Fprint(w, string(json_sp))
}

func (dg Diag) format(w http.ResponseWriter) {
  json_dg, err := json.MarshalIndent(dg, "", "    ")
  if err != nil {
    http.Error(w, "Could not encode the object", http.StatusBadRequest)
  }
  fmt.Fprint(w, string(json_dg))
}
