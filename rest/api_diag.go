package rest

import (
  "net/http"
  "time"
  "strings"
)

type Time struct {
  StartTime time.Time
  Uptime    float64
}

func (t *Time) getStatus(w http.ResponseWriter, url string) int {

  result, err := http.Get(url)
  if err != nil {
    http.Error(w, "Could not retrieve from server", http.StatusBadRequest)
    t.StartTime = time.Now()
  }
  t.Uptime = time.Since(t.StartTime).Seconds()
  return result.StatusCode
}

func HandlerDiag (w http.ResponseWriter, r *http.Request) {

  timer := Time {
    StartTime: time.Now(),
  }

  parts   := strings.Split(r.URL.Path, "/")
  version := parts[2]
  var dg Diag

  url_gbif           := "https://api.gbif.org/v1/species/"
  url_restcountries  := "https://restcountries.eu/rest/v2/"

  dg.Gbif           = timer.getStatus(w, url_gbif)
  dg.Restcountries  = timer.getStatus(w, url_restcountries)

  dg.Version = version

  dg.Uptime = timer.Uptime

  dg.format(w)
}
