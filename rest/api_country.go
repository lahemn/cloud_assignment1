package rest

import (
  "net/http"
  "encoding/json"
  "strings"
  "fmt"
)

func HandlerCountry (w http.ResponseWriter, r *http.Request) {
  parts  := strings.Split(r.URL.Path, "/")        // /conservation/v2/country/..
  country_identifier  := parts[4]                 // ISO number user writes
  limit               := string(r.URL.RawQuery)   // limit of occurrences
  fmt.Println(parts[4], limit)
  var co Country                                  // Creates a Country struct
  var sp Speciesid                                // Creates a Speciesid struct

    // retrieves the url we want to use
  url_country := "https://restcountries.eu/rest/v2/alpha/" + country_identifier
  url_species := "https://api.gbif.org/v1/occurrence/search?country=" +
                  country_identifier + "&" + limit

    // Checks if we manages to decode the web api endpoints into struct object
  checkDecoding(w, json.NewDecoder(recieveApi(w, url_country)).Decode(&co))
  checkDecoding(w, json.NewDecoder(recieveApi(w, url_species)).Decode(&sp))

    /* Loops through species struct object to see if its list of species
        contains any duplicate. Otherwise, add its content
        to country struct object                                          */
  for _, s := range sp.Results {
    if co.duplicate(s.Species) != true {
      co.Species = append(co.Species, s.Species)
      co.SpeciesKey = append(co.SpeciesKey, s.SpeciesKey)
    }
  }

    // Format Country structs' content into json
  co.format(w)

}
